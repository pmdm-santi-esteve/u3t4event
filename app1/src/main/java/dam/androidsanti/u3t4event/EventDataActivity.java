package dam.androidsanti.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private EditText etPlace;
    private String priority = "Normal";
    private String[] month;
    private int Checked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));

        //TODO: Ex1.3 Inicializamos los valores en caso de que estemos editando un evento
        if(inputData.containsKey("EMonth")){

            etPlace.setText(inputData.getString("EventPlace"));
            dpDate.updateDate(inputData.getInt("EYear"), inputData.getInt("EMonth2"), inputData.getInt("EDay"));
            tpTime.setHour(inputData.getInt("EHour"));
            tpTime.setMinute(inputData.getInt("EMinute"));
            rgPriority.check(inputData.getInt("EChecked"));
        }

        //TODO: Ex1.2 Cargamos el string-array
        Resources res = getResources();
        month = res.getStringArray(R.array.monthsArray);
    }

    private void setUI(){

        tvEventName = (TextView) findViewById(R.id.tvEventName);

        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        dpDate = (DatePicker) findViewById(R.id.dpDate);
        tpTime = (TimePicker) findViewById(R.id.tpTime);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);
        etPlace = (EditText) findViewById(R.id.etPlace);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData =  new Bundle();
        int resultcode = RESULT_CANCELED;

        switch (view.getId()){
            case R.id.btAccept:

                //TODO: Ex1.3 Enviammos a la activity principal toda la informacion necesaria
                eventData.putString("EventPlace",""+etPlace.getText());
                eventData.putString("EventPriority",priority);
                eventData.putInt("EDay", dpDate.getDayOfMonth());
                eventData.putString("EMonth", month[dpDate.getMonth()]);
                eventData.putInt("EMonth2", dpDate.getMonth());
                eventData.putInt("EYear", dpDate.getYear());
                eventData.putInt("EHour", tpTime.getHour());
                eventData.putInt("EMinute", tpTime.getMinute());
                eventData.putInt("EChecked", Checked);

                resultcode = RESULT_OK;
                break;

         //TODO: Ex1.1 Cuando pulsemos el botón “Cancel” se conservaran los datos
            case R.id.btCancel:
            eventData.putString("EventData", "");
                resultcode = RESULT_CANCELED;
                break;
        }

        activityResult.putExtras(eventData);
        setResult(resultcode, activityResult);
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {

        Checked = i;

        switch (i){

            case R.id.rbLow:
                priority = "Low";
                break;

            case R.id.rbNormal:
                priority = "Normal";
                break;

            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }
}
