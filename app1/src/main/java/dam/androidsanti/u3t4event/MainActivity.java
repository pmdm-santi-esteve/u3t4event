package dam.androidsanti.u3t4event;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI(savedInstanceState);
    }

    //TODO: Ex1.3 Guardamos el bundle
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        if (bundle!=null){

            savedInstanceState.putBundle("bundle", bundle);
        }

    }

    @SuppressLint("SetTextI18n")
    private void setUI(@NonNull Bundle savedInstanceState){

        etEventName = (EditText) findViewById(R.id.etEventName);
        tvCurrentData = (TextView) findViewById(R.id.tvCurrentData);

        tvCurrentData.setText("");

        if(bundle == null){
            bundle = new Bundle();
        }

        //TODO: Ex1.3 Restauramos los valores guardados en el el bundle mediante el onSaveInstanceState

        if (savedInstanceState != null) {
            bundle = savedInstanceState.getBundle("bundle");
            tvCurrentData.setText("Place: "+bundle.getString("EventPlace")+"\n"+
                    "Priority: "+bundle.getString("EventPriority")+"\n"+
                    "Date: "+bundle.getInt("EDay")+" "+bundle.getString("EMonth")+" "+bundle.getInt("EYear")+"\n"+
                    "Hour: "+bundle.getInt("EHour")+":"+bundle.getInt("EMinute"));
        } else {

            tvCurrentData.setText("");
        }
    }

    public void editEventData(View v){

        Intent intent = new Intent(this, EventDataActivity.class);

        if(tvCurrentData.getText().length() == 0){

            bundle = new Bundle();
        }
        bundle.putString("EventName", etEventName.getText().toString());
        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST && resultCode == RESULT_OK){
            bundle = data.getExtras();
            tvCurrentData.setText(data.getStringExtra("EventPlace")+"\n"+data.getStringExtra("EventPriority")+"\n"+
                    data.getStringExtra("EventDate")+"\n"+data.getStringExtra("EventTime"));
            tvCurrentData.setText("Place: "+data.getStringExtra("EventPlace")+"\n"+
                    "Priority: "+data.getStringExtra("EventPriority")+"\n"+
                    "Date: "+data.getIntExtra("EDay", 0)+" "+data.getStringExtra("EMonth")+" "+data.getIntExtra("EYear",00)+"\n"+
                    "Hour: "+data.getIntExtra("EHour",00)+":"+data.getIntExtra("EMinute",00));
        }
    }
}
