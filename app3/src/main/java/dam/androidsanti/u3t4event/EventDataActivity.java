package dam.androidsanti.u3t4event;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener,
        TimeDialogFragment.OnTimeSetPickerListener, DatePickerFragment.OnDatePickerSetListener {

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private Button btAccept;
    private Button btCancel;
    private EditText etPlace;
    private String priority = "Normal";
    private String[] month;
    private int Checked;
    private Button bDate;
    private Button bTime;
    private EditText etPlannedDate;
    private EditText etTime;

    private Bundle inputData;

    private int dpday;
    private int dpmonth;
    private int dpyear;
    private int tphour;
    private int tpminute;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));

        if(inputData.containsKey("EMonth")){

            etPlace.setText(inputData.getString("EventPlace"));
            rgPriority.check(inputData.getInt("EChecked"));
            etPlannedDate.setText(inputData.getInt("EDay")+"/"+inputData.getInt("EMonth2")+"/"+inputData.getInt("EYear"));
            etTime.setText(inputData.getInt("EHour")+":"+inputData.getInt("EMinute"));
        }

        Resources res = getResources();
        month = res.getStringArray(R.array.monthsArray);

    }

    private void setUI(){

        tvEventName = (TextView) findViewById(R.id.tvEventName);

        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);
        etPlace = (EditText) findViewById(R.id.etPlace);
        etPlannedDate = (EditText) findViewById(R.id.etPlannedDate);
        etTime = (EditText) findViewById(R.id.etTime);
        bDate = (Button) findViewById(R.id.bDate);
        bTime = (Button) findViewById(R.id.bTime);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

    }

    @Override
    public void onClick(View view) {

        Intent activityResult = new Intent();
        Bundle eventData =  new Bundle();
        int resultcode = RESULT_CANCELED;

        switch (view.getId()){

            case R.id.btAccept:

                eventData.putString("EventPlace",""+etPlace.getText());
                eventData.putString("EventPriority",priority);
                eventData.putInt("EDay", dpday);
                eventData.putString("EMonth", month[dpmonth-1]);
                eventData.putInt("EMonth2", dpmonth);
                eventData.putInt("EYear", dpyear);
                eventData.putInt("EHour", tphour);
                eventData.putInt("EMinute", tpminute);
                eventData.putInt("EChecked", Checked);

                resultcode = RESULT_OK;
                activityResult.putExtras(eventData);
                setResult(resultcode, activityResult);
                finish();
                break;

            case R.id.btCancel:

                eventData.putString("EventData", "");
                activityResult.putExtras(eventData);
                setResult(resultcode, activityResult);
                finish();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int i) {

        Checked = i;

        switch (i){

            case R.id.rbLow:
                priority = "Low";
                break;

            case R.id.rbNormal:
                priority = "Normal";
                break;

            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }

    public void showTimePickerDialog(View v) {

            DialogFragment newFragment = new TimeDialogFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");

    }

    @Override
    public void onTimeSetPicked(int hour, int minute) {

        tphour = hour;
        tpminute = minute;

        etTime.setText(hour+":"+minute);
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment2 = new DatePickerFragment();
        newFragment2.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onDateSetPicked(int day, int mounth, int year) {

        dpday = day;
        dpmonth = mounth+1;
        dpyear = year;

        etPlannedDate.setText(dpday+"/"+dpmonth+"/"+dpyear);
    }
}
