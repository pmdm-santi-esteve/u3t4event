package dam.androidsanti.u3t4event;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class TimeDialogFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private OnTimeSetPickerListener listener;

    public TimeDialogFragment() {

    }

    public interface OnTimeSetPickerListener{

        void onTimeSetPicked(int hour, int minute);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

        listener = (OnTimeSetPickerListener) getActivity();

        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        listener.onTimeSetPicked(hourOfDay, minute);
    }
}